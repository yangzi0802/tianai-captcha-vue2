## 后台demo仓库地址
> [tianai-captcha-demo](https://gitee.com/tianai/tianai-captcha-demo)


## 环境

- [ ] vue2
- [ ] nodejs 16+
## 配置选项

| 参数 | 说明 | 类型 | 可选值 | 默认值 |
| --- | --- | --- | --- | --- |
| type | 验证码类型 | String | SLIDER、ROTATE_DEGREE、CONCAT、ROTATE | SLIDER |
| title | 验证码标题 | String | - | 拖动滑块完成拼图 |
| titleAlign | 标题水平方向（justify-content 选值） | String | flex-start、center、flex-end | center |
| titleColor | 验证码标题字体颜色 | String |  | rgb(230, 162, 60) |
| titleFontSize | 验证码标题字体 | Number | - | 15 |
| titleWeight | 标题字体粗细 | String | - | bold |
| bgImage | 背景图片 | String | - | - |
| width | 验证码宽度 | Number | - | 300 |
| height | 验证码高度 | Number | - | 300 |
| margin | 验证码与外边界边距 | Number | - | 20 |
| borderRadius | 圆角 | Number | - | 10 |
| moveBtnImg | 滑块背景图 | String | - | - |
| moveBtnImgWidth | 滑块图片宽度 | Number | - | 53 |
| moveBtnImgHeight | 滑块图片高度 | Number | - | 38 |
| trackerColor | 滑块滑过的样式 | String | - | #ef9c0d |
| trackerBgColor | 滑块滑过的背景样式 | String | - | #f7b645 |
| @closeCaptcha | 关闭验证码事件 | function |  |  |
| @refreshCaptcha | 刷新验证码事件 | function |  |  |
| @validateCaptcha | 验证事件 | function |  |  |

## 


