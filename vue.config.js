const {defineConfig} = require('@vue/cli-service')
module.exports = defineConfig({
    transpileDependencies: true,
    lintOnSave: false,
    publicPath:  "/",
    devServer: {
        host: 'localhost',
        port: 80,
        open: true,
        proxy: {
            '/': {
                target: 'http://localhost:8080',
                changeOrigin: true,
                ws: false,
                liveReload: true,
                pathRewrite: {
                    '^/': ''
                }
            }
        },
    },
})
