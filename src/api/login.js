import request from '@/utils/request'

export function gen(type) {
    return request({
        url: '/gen',
        method: 'get',
        params: {type}
    })
}

export function check(captchaId, data) {
    return request({
        url: '/check?id=' + captchaId,
        method: 'post',
        data: data
    })
}
